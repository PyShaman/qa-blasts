import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class BasicTest(unittest.TestCase):

    def setUp(self):
        global driver
        driver = webdriver.Chrome(executable_path = 'chromedriver.exe')
        driver.get('https://github.com')

    def tearDown(self):
        driver.quit()

    def test_login_with_bad_credentials(self):
        self.wait_for_element(By.ID, 'user[login]')
        username = driver.find_element(By.ID, 'user[login]')
        username.send_keys('test1')
        self.wait_for_element(By.ID, 'user[email]')
        email = driver.find_element(By.ID, 'user[email]')
        email.send_keys('email@email.com')
        self.wait_for_element(By.ID, 'user[password]')
        password = driver.find_element(By.ID, 'user[password]')
        password.send_keys('this_is_p4zzw0rD1!')
        self.wait_for_element(By.CSS_SELECTOR, 'button[type="submit"]')
        submit_button = driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]')
        submit_button.click()
        self.wait_for_element(By.CSS_SELECTOR, 'div[class="flash flash-error my-3"]')
        error_message = driver.find_element(By.CSS_SELECTOR, 'div[class="flash flash-error my-3"]')
        assert error_message.text == 'There were problems creating your account.'

    # helper methods
    def wait_for_element(self, _by, locator):
        try:
            WebDriverWait(driver, 10).until(ec.presence_of_element_located((_by, locator)))
        except ec.NoSuchElementException as err:
            print(err)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
